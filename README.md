Visual Studio Code MSSnippets extension
===================================

![Visual Studio Marketplace Version](https://img.shields.io/visual-studio-marketplace/v/JesterMeijin.maniascript-snippets?label=Version)
![Visual Studio Marketplace Downloads](https://img.shields.io/visual-studio-marketplace/d/JesterMeijin.maniascript-snippets?label=Downloads)
![Visual Studio Marketplace Installs](https://img.shields.io/visual-studio-marketplace/i/JesterMeijin.maniascript-snippets?label=Install)
![Visual Studio Marketplace Rating](https://img.shields.io/visual-studio-marketplace/stars/JesterMeijin.maniascript-snippets?label=Rating)

Integrates code snippets for ManiaScript into Visual Studio Code.

## Features

MSSnippets adds snippet support for ManiaScript. This makes it easier to enter repetitive code patterns, such as loops or conditional statements (c.f. [Snippets in Visual Studio Code](https://code.visualstudio.com/docs/editor/userdefinedsnippets)).  


![MSSnippets](https://gitlab.com/JesterMeijin/vscode-extension-mssnippets/uploads/96c6bc9b09282e5497d74783811c68cf/MSSnippets.png)  
![MSSnippets](https://gitlab.com/JesterMeijin/vscode-extension-mssnippets/uploads/9385e67e791ec1df49643edf243075c6/MSSnippets.gif)  

## Release Notes

Check [CHANGELOG.md](CHANGELOG.md)